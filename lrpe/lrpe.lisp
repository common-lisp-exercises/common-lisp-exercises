;; 2001 July 16 Lisp Programming Exercises
;; Lisp Recursive Programming Exercises

;; 1. Write a Lisp macro mycase that translates the following macro
;;    call. Assume the input will be error free. The input lists can be
;;    any length. You must document your solution.
;;
;;       (mycase (C1 C2 ... Cn) (P1 P2 ... Pn))
;;
;; translates to the following
;;
;;       (mycond (C1 P1) (C2 P2) ... (Cn Pn))

(defmacro mycase (conditions expressions)
  `(cond ,@(mapcar (function list) conditions expressions)))

(macroexpand-1 '(mycase (C1 C2 \... Cn) (P1 P2 \... Pn)))
;; --> (cond (c1 p1) (c2 p2) (|...| |...|) (cn pn))
;;     t



;; 2. Write a recursive function, (defun nth (pos list) ???), that
;;    returns the n'th item from a list. Assume the list has at least n
;;    items. (nth 1 aList) is to return the first item in aList.

(defun .nth (pos list)
  (cond
    ((< pos 1)   (error "POS=~A too small" pos))
    ((= pos 1)   (first list))
    ((endp list) list)
    (t           (.nth (1- pos) (cdr list)))))

(list (.nth 1 '(2 3 4))
      (.nth 2 '(2 3 4))
      (.nth 3 '(2 3 4))
      (.nth 4 '(2 3 4)))
;; --> (2 3 4 nil)



;; 3. Write a recursive function, (defun index (???) ???), that returns a
;;    matrix element – A[I1,I2,...,In] – the element at I1 in the first
;;    dimension, I2 in the second dimension, etc. A call of the form
;;    (index array I1 I2 ... In) would be used. Assume caller will not
;;    have out of bound indices. There is no fixed size for the number of
;;    dimensions of the matrix. Use the function nth from Question 2.
;;    Assume index value 1 is the first item in the corresponding
;;    dimension. Do not use length, last, butlast, etc.., stick to first
;;    (or car) and rest (or cdr).


;; badass answer: in CL, matrices are conveniently represented as 2D array.
;; such as: #2A((1 0) (0 -1))  and AREF is used to access the slots,
;; as for a multidimensional arrays in general.

(setf (fdefinition 'index) (fdefinition 'aref))

;; or, if we want to offset the indices, start by reading:
;; https://www.cs.utexas.edu/users/EWD/transcriptions/EWD08xx/EWD831.html
;; then:

(defun index (array &rest indices)
  (apply (function aref) array
         (mapcar (function 1-) indices)))


(index #4A((((1      2     3)     (4     5     6))
            ((a      b     c)     (d     e     f))
            ((11     22    33)    (44    55    66))
            ((aa     bb    cc)    (dd    ee    ff)))
           (((1001   1002  1003)  (1004  1005  1006))
            ((100a   100b  100c)  (100d  100e  100f))
            ((1011   1022  1033)  (1044  1055  1066))
            ((10aa   10bb  10cc)  (10dd  10ee  10ff))))
       2 1 2 3)
;; --> 1006


;; But the purpose of the exercise seems to be to implement
;; multidimentionals arrays using lists.  Notice that matrices always
;; have only 2 dimensions.

;; So if we implement multidimensional arrays as lists and sublists,
;; we can implement index as:

(defun index (array &rest indices)
  (cond
    ((endp indices) array)
    (t (apply (function index) (.nth (first indices) array) (rest indices)))))

(index '((((1      2     3)     (4     5     6))
          ((a      b     c)     (d     e     f))
          ((11     22    33)    (44    55    66))
          ((aa     bb    cc)    (dd    ee    ff)))
         (((1001   1002  1003)  (1004  1005  1006))
          ((100a   100b  100c)  (100d  100e  100f))
          ((1011   1022  1033)  (1044  1055  1066))
          ((10aa   10bb  10cc)  (10dd  10ee  10ff))))
       2 1 2 3)
;; --> 1006




;; 4. Write a macro function our-if that translates the following macro
;;    calls.
;;
;;         (our-if a then b)
;;
;;    translates into
;;
;;         (cond (a b))
;;
;;
;;         (our-if a then b else c)
;;
;;    translates into
;;
;;         (cond (a b) (t c))


(defmacro our-if (a then b &optional else (c nil cp))
  (declare (ignore then else))
  `(cond (,a ,b) ,@(when cp `((t ,c)))))

(macroexpand-1 '(our-if a then b))
;; (cond (a b))
;; t

(macroexpand-1 '(our-if a then b else c))
;; (cond (a b) (t c))
;; t

;; But note that it is totally useless to test for cp, since cond
;; returns nil when no clause apply.

(defmacro our-if (a then b &optional else c)
  (declare (ignore then else))
  `(cond (,a ,b) (t ,c)))


(macroexpand-1 '(our-if a then b))
;; (cond (a b) (t nil))
;; t


;; Compare
(macroexpand '(cond (a b) (t nil)))
(macroexpand '(cond (a b)))
;; or better:
(disassemble (compile nil '(lambda () (cond (a b) (t nil)))))
(disassemble (compile nil '(lambda () (cond (a b)))))



;; 5. Write your own recursive version myMaplist of the maplist
;;    function. If possible, do not define additional functions but it is
;;    better to have them with a correct and commented function than have
;;    an incorrect function. Maplist, which can have any number of lists
;;    as arguments, terminates when one of the input lists becomes
;;    empty. A function with a fixed number of arguments is not
;;    acceptable.

;; maplist (et al.) takes at least one list.
(defun mymaplist (fun list1 &rest lists)
  (let ((all (cons list1 lists)))
    (if (some (function endp) all)
        '()
        (cons (apply fun all) ; mymapcar would use (mapcar (function car) all) instead of all here.
              (apply (function mymaplist) fun
                     (mapcar (function rest) all))))))


(mymaplist #'append '(1 2 3 4) '(1 2) '(1 2 3))
;; --> ((1 2 3 4 1 2 1 2 3) (2 3 4 2 2 3))
(mymaplist #'(lambda (x) (cons 'foo x)) '(a b c d))
;; --> ((foo a b c d) (foo b c d) (foo c d) (foo d))
(mymaplist #'(lambda (x) (if (member (car x) (cdr x)) 0 1)) '(a b a c d b c))
;; --> (0 0 1 0 1 1 1)



;; 6. How would multi-dimensional matrices be implemented in Lisp? Define
;;    the operation 'index' which has an array and an index list as
;;    parameters; the function is to return the indexed array element.

;; See exercice 3.



;; 7. Write simple lisp functions such as the following. Take into
;;    account lists which are too short.
;;
;;     • (remove-first '( a b c ...)) -> ( b c ...) --- remove the first item from the list.
;;     • (remove-second '(a b c ...)) -> (a c ...) -- remove the second item from the list.
;;     • (insert-as-second 'b '(a c ...)) -> (a b c ...) --- insert as the second element.

(setf (fdefinition 'remove-first) (fdefinition 'rest))

(remove-first '(a b c \...))
;; --> (b c |...|)

(defun remove-second (list)
  (cons (first list) (rest (rest list))))

(remove-second '(a b c \...))
;; --> (a c |...|)

(defun insert-as-second (element list)
  (list* (first list) element (rest list)))

(insert-as-second 'b '(a c \...))
;; --> (a b c |...|)



;; 10. Write a recursive function, (defun nth (???) ???), that returns
;;     the n'th item from a list. Assume the list has at least n
;;     items. (nth 1 aList) is to return the first item in aList.

;; See: exercice 2.



;; 11. Write a recursive function, (defun diagOf(theMatrix) ...) to
;;     return the diagonal of a square matrix. Assume the input is error
;;     free. You may write support functions. Do not use global
;;     variables. Do not use let, prog and similar features to introduce
;;     local variables; use only parameters to functions as local
;;     variables.

;; In Common Lisp, we use 2D arrays and ARRAY-DIMENSION, AREF, etc:

(defun diagonal-of-submatrix (matrix index)
  (if (< index (array-dimension matrix 0))
      (cons (aref matrix index index)
            (diagonal-of-submatrix matrix (1+ index)))
      '()))

(defun diagof (thematrix)
  (diagonal-of-submatrix thematrix 0))

(diagof #2A((11 12 13)
            (21 22 23)
            (31 32 33)))
;; --> (11 22 33)


;; But since the exercice wants us to use lists of lists:

(defun diagonal-of-submatrix (matrix index)
  (if (<= index (length (first matrix)))
      (cons (.nth index (first matrix))
            (diagonal-of-submatrix (rest matrix) (1+ index)))
      '()))

(defun diagof (thematrix)
  (diagonal-of-submatrix thematrix 1))

(diagof '((11 12 13)
          (21 22 23)
          (31 32 33)))
;; --> (11 22 33)



;; 12. Write a your own recursive version myMaplist of the maplist
;;     function. If possible, do not define additional functions but it
;;     is better to have them with a correct and commented function than
;;     have an incorrect function. Hints: Recall the functions some and
;;     every. Maplist terminates when one of the input lists becomes nil.

;; See: exercise 5.



;; 13. Write a recursive version of reverse
;;
;;       (defun myrev (theList) ... )
;;
;;     using LAST and BUTLAST. Check it by reversing a list twice to see
;;     if it equals the original. O nly the top level is reversed; so
;;     reversing (A B (C D)) produces ((C D) B A). Use only functions
;;     from Chapters 1 to 6 inclusive.

;; This is an horrible function:
;; - last is O(n) in time.
;; - butlast is O(n) in time and in space
;; - append is O(n) in time and in space
;; therefore (append (last thelist) (myrev (butlast thelist)))
;; calls CDR 2N times, and allocates 1+N+N-1=2N new cons cells,
;; so myrev is O(N*4N) = O(4N²) with N = (length list).

(defun myrev (thelist)
  (if (endp thelist)
      '()
      (append (last thelist) (myrev (butlast thelist)))))

(myrev '(1 2 3 4))
;; --> (4 3 2 1)

(let ((list '(1 2 3 4)))
  (equal list (myrev (myrev list))))
;; --> t



;; A better reverse function DOES NOT USE last and butlast, and is O(N)!
(defun myrev (list)
  (labels ((rev (list reversed)
             (if (endp list)
                 reversed
                 (rev (cdr list)
                      (cons (car list) reversed)))))
    (rev list '())))
(myrev '(1 2 3 4))
;; --> (4 3 2 1)



;; 14. Write a recursive version of reverse using LAST and BUTLAST where
;;     every level of a list is reversed. For example, reversing (A B (C
;;     D)) produces ((D C) B A).


;; cf. exercise 13 about last and butlast.

(defun myrevrec (thelist)
  (if (atom thelist)
      thelist
      (cons (myrevrec (first (last thelist)))
            (myrevrec (butlast thelist)))))

;; now, instead of using endp (which tests for non-dotted lists),
;; we test for atom, so that (myrevrec atom) --> atom.

(myrevrec '(A B (C  D)))
;; --> ((d c) b a)



;; 15. The function READ reads the next s-expression from the input and
;;     returns it. Experiment by typing an s-expression after entering
;;     (setq x (read)) and then check the value of x. Experiment with
;;     (setq x (cons (read) (read)) as well. Write the following
;;     function. to read a sequence of s-expressions as defined in the
;;     following. Assume correct input.
;;
;;       (defun create-symbol-and-prop () ... )
;;
;;       Input ::= aLispSymbol aValue PropertyList ;
;;       PropertyList ::=  ( "endp" , aPropName aPropValue PropertyList) ;
;;       aLispSymbol ::= is a Lisp symbol
;;       aValue ::= any s-expression
;;       aPropName ::= is the name of a property
;;       aPropValue ::= any s-expression
;;
;;     Example Input
;;
;;       vertex (3.0 4.0) colour black change (penny 3 dime 4 looney 6)
;;       endp
;;
;;     The result of the function is to create the global variable
;;     "vertex" assign it the value "(3.0 4.0)" and give it the property
;;     "colour" with value "black", and the property "change" with the
;;     value "(penny 3 dime 4 looney 6)".
;;
;;     Use recursion to process the property list data. Except for READ
;;     do not use material beyond Chapter 7. Use PUTPROP as defined in
;;     exercise 1 chapter 7 (page 121). You must define your own
;;     function.
;;
;;     Verify your function using SYMBOL-PLIST and GET.



;; 16. Do exercise 4 in Wilensky Chapter 8 (page 140). For the solution
;;     I'm looking do not write support functions. Instead use a LAMBDA
;;     form (Chapter 9). Hints: Consider the following function which
;;     uses the keyword "&rest" (only thing from Chapter 12) to gather a
;;     sequence of parameters into a single list.
;;
;;        (defun first-of-each (&rest sequence-of-lists)
;;          (mapcar 'car sequence-of-lists))
;;        (first-of-each '(1 2 3) '(a b c) '(d e f))
;;         (1 a d)

;; 17. Do exercise 5 in Wilensky Chapter 12 (page 220). The only thing
;;     used from that chapter is the &rest keyword (see exercise 3
;;     above). Otherwise all you need is material from Chapter 8 and
;;     earlier. Write a fully recursive version. Do not use functionals.

;; 18. Construct a macro definition, foreach, to do the following.  Form
;;     1 applies a function to each item in a list that satisfies a given
;;     predicate (test) and returns nil otherwise.
;;
;;       (foreach (item in list) (apply func) (when (test)))
;;         ==> (mapcar '(lambda (item)
;;                        (cond ((test item)
;;                               (funcall func item)))) list)
;;
;;     Form 2 like form1 except all nil values are removed.
;;
;;       (foreach (item in list) (save func) (when (test)))
;;            ==> (apply 'append
;;                 (mapcar '(lambda (item)
;;                            (cond ((test item)
;;                                   (list (funcall func item))))) list))
;;
;;     Example 1: assuming b1 = (11 20 33 40 55 60)
;;
;;        (foreach (number in b1) (apply '1+) (when (evenp)))
;;
;;     when executed gives (nil 21 nil 41 nil 61)
;;
;;     Example 2:
;;
;;           (foreach (number in b1) (save '1+) (when (evenp)))
;;
;;     when executed gives (21 41 61)

;; 19. Do a variation of exercise 7 in Wilensky, Chapter 6 (page 110). Do
;;     only the recursive version. Make sure you "sub-splice" every
;;     occurrence of the second parameter.

;; 20. Write a recursive function, (defun myinter (list1 list2) ... ),
;;     that computes the set intersection of list1 and list2. Use the
;;     member function.

;; 21. Define your versions of the functions some (call it mysome) and
;;     every (call it myevery) in exercise 7 in Wilensky Chapter 8 (page
;;     140-141).


;; 22. Modify the fully recursive definition of sub-splice from above
;;     that accepts the keyword :everywhere (Chapter 12). If the argument
;;     is non-nil, then your function will do sub-splice everywhere in
;;     the input list. If the argument is nil, then your function will do
;;     sub-splice only at the top level of the list.


;; 23. Write a macro that expands (select smallInt from aList) into
;;     (selector aList) where selector is one of the following. For all
;;     other values of smallInt return NIL.
;;
;;         smallInt selector
;;           1        first
;;           2        second
;;           3        third
;;           4        fourth


;; 24. The following program countRemove removes all instances of the
;;     item from the list. Complete the program so it returns as its
;;     first value the modified list and as its second value a count of
;;     the number of replacements.
;;
;;        (defun countRemove (item list)
;;        (cond ((atom list) list)
;;              ((equal (first list) item) (countRemove item (rest list)))
;;                (t (cons (first list) (countRemove item (rest list))))))

;; 25. Write one macro function cfunc that translates the following macro
;;     calls.
;;
;;        (cfunc fname (parm))     translates into (function fname (parm))
;;        (cfunc fname (parm) int) translates into (int function fname (parm))


;; 26. Write a recursive function, (defun flatten(theList) ...) to return
;;     all the atoms in the theList as a single level list while
;;     retaining their order.
;;
;;           For example (A (B (C D) E) (F G)) becomes (A B C D E F G).
;;
;;     Assume the input is error free. You may write support
;;     functions. Do not use global variables. Do not use let, prog and
;;     similar features to introduce local variables; use only parameters
;;     to functions as local variables.

;; 27. Program insert sort and bubble sort (with two values the returned
;;     list and whether a swap was done).

;; 28. Define a function to merge two sorted numeric lists.

(defun merge-list-of-reals (lor1 lor2)
  (loop
    :while (and lor1 lor2)
    :if (< (first lor1) (first lor2))
      :collect (pop lor1) :into merged
    :else
      :collect (pop lor2) :into merged
    ;; Finally, (or (endp lor1) (endp lor2));
    ;; we use concatenate to make a copy of lor1+lor2
    ;; so we return an entirely fresh list.
    :finally (return (nconc merged (concatenate 'list lor1 lor2)))))


;; now, for a badly recursive function:

(defun recursive-merge-list-of-reals (lor1 lor2)
  (cond
    ((endp lor1) lor2)
    ((endp lor2) lor1)
    ((< (first lor1) (first lor2))
     (cons (first lor1)
           (recursive-merge-list-of-reals (rest lor1) lor2)))
    (t
     (cons (first lor2)
           (recursive-merge-list-of-reals lor1 (rest lor2))))))

;; and finally, a better tail-recursive function:

(defun recursive-merge-list-of-reals (lor1 lor2)
  (labels ((accumulate (lor1 lor2 result)
             (cond
               ((endp lor1)
                (if lor2
                    (accumulate lor1 (rest lor2) (cons (first lor2) result))
                    (nreverse result)))
               ((endp lor2)
                (accumulate (rest lor1) lor2 (cons (first lor1) result)))
               ((< (first lor1) (first lor2))
                (accumulate (rest lor1) lor2 (cons (first lor1) result)))
               (t
                (accumulate lor1 (rest lor2) (cons (first lor2) result))))))
    (accumulate lor1 lor2 ())))


;; 29. Define a function to merge sort a numeric list


;; 30. Define functions for the prefix, suffix and sublist of a list.
;;
;;     • by index position: prefix first n, suffix last n, sublist
;;       lowerBound to upperBound inclusive
;;
;;     • boolean to return true if list_1 is a prefix, suffix or sublist
;;       of list_2 (compare with Prolog)


;; 31. Write a recursive function, remove-nth that removes the n’th
;;     element from every list at all levels. Counting begins at 1. You
;;     cannot use any implicitly recursive function, such as mapcar,
;;     length,etc. Use car, cdr and cons for the basic list operations and use
;;     cond for conditional expressions. You are permitted to and will need
;;     to write recursive support functions.
;;
;;         Precondition: n ≥ 1.
;;         (defun remove-nth (n list) ;; You supply the rest

(defun remove-nth (n list)
  (check-type n (integer 1))
  (check-type list list)

  )

;; 32. Using the following two functions, that you do not have to
;;     implement,
;;
;;       prefix(p, list) = list(1 .. min(p, #list))
;;       suffix(q, list) = list(max(1, q) .. #list)
;;
;;     write a functional program sublist-all(p, q, list-of-lists) that
;;     returns a list of the sublists of each of the lists in
;;     list-of-lists. The definition is have no explicit recursion,
;;     including within any lambda functions. The definition of a sublist
;;     is the following.
;;
;;         sublist(p, q, list) = list(max(1,p) .. min(q, #list))
;;
;;      Example: (sublist-all 2 3 ‘((1 2 3 4) (1 2) (1) ((1) (2) (3) (4))
;;                  => ((2 3) (2) nil ((2) (3)))
;;
;;         (defun sublist-all (p, q, list-of-lists) ;; You supply the rest
