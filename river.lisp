;; You are standing in front of a river in which little baskets are
;; swimming. In each basket there is a piece of paper on which one single
;; letter or digit or symbol is written. There might be duplicates.
;;
;; Now you have a message in mind that you would like to write with the
;; letters from the baskets.
;;
;; On the other side of the river there is some kind of traffic
;; light. When it signals a red light then no more baskets with letters
;; will come.
;;
;; Your task is to write an efficient program in Haskell which checks if
;; your message can be written with the letters that the river provides.


(defun message-validp (message baskets)
  (let ((letters (make-hash-table :test (function eql)))
        (total   0))
    (loop
      :for letter :across message
      :do (incf (gethash letter letters 0))
          (incf total))
    (with-input-from-string (river baskets)
      (loop
        :for letter := (read-char river nil :red)
        :when (eq letter :red)
          :do (return-from message-validp (zerop total))
        :when (plusp (gethash letter letters 0))
          :do (decf (gethash letter letters))
              (when (zerop (decf total))
                (return-from message-validp t))))))

(defvar *baskets* "
You are standing in front of a river in which little baskets are
swimming. In each basket there is a piece of paper on which one single
letter or digit or symbol is written. There might be duplicates.

Now you have a message in mind that you would like to write with the
letters from the baskets.

On the other side of the river there is some kind of traffic
light. When it signals a red light then no more baskets with letters
will come.

Your task is to write an efficient program in Haskell which checks if
your message can be written with the letters that the river provides.
")


(assert (equal (list (message-validp "hello world" *baskets*)
                     (message-validp "wwwww wwwww" *baskets*)
                     (message-validp "wwwww wwwww wwwww" *baskets*)
                     (message-validp "C'est l'été" *baskets*)
                     (message-validp "abc" "xxxbcax")
                     (message-validp "abbc" "cbda"))
               '(t t nil nil t nil)))
