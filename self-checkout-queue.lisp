;; There is a queue for the self-checkout tills at the supermarket. Your task is
;; write a function to calculate the total time required for all the customers to
;; check out!
;;
;; Input:
;;
;;     customers: an array of positive integers representing the queue. Each
;;     integer represents a customer, and its value is the amount of time they
;;     require to check out.
;;
;;     n: a positive integer, the number of checkout tills.
;;
;; Output:
;;
;;     The function should return an integer, the total time required.
;;
;; Important:
;;
;;     Please look at the examples and clarifications below, to ensure you understand
;;     the task correctly
;;
;; Examples:
;;
;;     queueTime [5,3,4] 1
;;     -- should return 12
;;     -- because when there is 1 till, the total time is just the sum of the times
;;
;;     queueTime [10,2,3,3] 2
;;     -- should return 10
;;     -- because here n=2 and the 2nd, 3rd, and 4th people in the
;;     -- queue finish before the 1st person has finished.
;;
;;     queueTime [2,3,10] 2
;;     -- should return 12
;;
;; Clarifications:
;;
;;     There is only ONE queue serving many tills, and
;;     The order of the queue NEVER changes, and
;;     The front person in the queue (i.e. the first element in the array/list)
;;     proceeds to a till as soon as it becomes free.
;;
;; N.B. You should assume that all the test input will be valid, as specified above.
;;
;; P.S. The situation in this kata can be likened to the
;;      more-computer-science-related idea of a thread pool, with relation to running
;;      multiple processes at the same time: https://en.wikipedia.org/wiki/Thread_pool

(defun position-extremum (vector lessp &key (start 0) (end nil) (key (function identity)))
  (loop
    :with extremum-position := nil
    :with extremum-value := nil
    :for position :from start :below (or end (length vector))
    :do (let ((position-value (funcall key (aref vector position))))
          (if extremum-position
              (when (funcall lessp extremum-value position-value)
                (setf extremum-position position
                      extremum-value position-value))
              (setf extremum-position position
                    extremum-value position-value)))
    :finally (return (values extremum-position extremum-value))))

(defun position-minimum (vector)
  (position-extremum vector (function >)))

(defun position-maximum (vector)
  (position-extremum vector (function <)))

(progn
  (assert (= 0 (position-minimum #(0 1 2 3))))
  (assert (= 1 (position-minimum #(1 0 2 3))))
  (assert (= 2 (position-minimum #(1 2 0 3))))
  (assert (= 3 (position-minimum #(1 2 3 0))))
  (assert (= 0 (position-maximum #(4 1 2 3))))
  (assert (= 1 (position-maximum #(1 4 2 3))))
  (assert (= 2 (position-maximum #(1 2 4 3))))
  (assert (= 3 (position-maximum #(1 2 3 4))))
  :success)

(defun queue-time (customers queue-count)
  (let ((queues (make-array queue-count :initial-element 0)))
    (loop :for customer-time :in customers
          :for selected-queue := (position-minimum queues)
          :do (incf (aref queues selected-queue) customer-time))
    (aref queues (position-maximum queues))))

(progn
  (assert (= 12 (queue-time '(5 3 4)    1)))
  (assert (= 10 (queue-time '(10 2 3 3) 2)))
  (assert (= 12 (queue-time '(2 3 10)   2)))
  :success)
