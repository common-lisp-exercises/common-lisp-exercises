#|

https://www.youtube.com/watch?v=9fZB4s38Ygg

We have 3 jugs, of capacities 12 l, 8 l, 5 l.
The first jug is full with 12 l.
We want to get a jug with 6 l.
What transfer operations are needed?

|#



(defun pour (capacities contents from to)
  (let ((transfered (min (- (aref capacities to)
                            (aref contents   to))
                         (aref contents from)))
        (new-contents (copy-seq contents)))
    (decf (aref new-contents from) transfered)
    (incf (aref new-contents to)   transfered)
    new-contents))

;; (pour #(12 8 5) #(12 0 0) 0 1)
;; --> #(4 8 0)
;; (pour #(12 8 5) #(4 8 0) 1 2)
;; --> #(4 3 5)
;; (pour #(12 8 5) #(4 3 5) 2 0)
;; --> #(9 3 0)
;; (pour #(12 8 5) #(9 3 0) 1 2)
;; --> #(9 0 3)
;; (pour #(12 8 5) #(9 0 3) 0 1)
;; --> #(1 8 3)
;; (pour #(12 8 5) #(1 8 3) 1 2)
;; --> #(1 6 5)
;; (pour #(12 8 5) #(1 6 5) 2 0)
;; --> #(6 6 0)
;;
;; Essentially what the video explains.  But this is entirely
;; unsatisfactory: we just give an answer, without explaining how we
;; achieve it, and without giving a way to find other such answers for
;; other such problems.
;;
;; I guess that before the 20th century, we didn't have much tools to
;; explain it. Nowadays, it's simplier: just build a graph of states, and
;; perform a breadth-first search.  Nowadays, a correct mathematical
;; answer to those problems should involve an algorithm!

(defun breadth-first-search-if (graph root predicate &key key)
  "
DO:     Implement the Breadth First Search algorithm on the given
        GRAPH, starting from the ROOT node, until the PREDICATE
        applied on the value of the KEY function applied to the node
        returns true.  (Default for KEY is IDENTITY).
RETURN: The goal node.
COMPLEXITY:  Time: O(|V|+|E|), Space: O(|V|)
"
  (let ((key (or key (function identity)))
        (head   '())
        (tail   '())
        (marks (make-hash-table :test (function equal))))
    (flet ((enqueue (node) (if (null head)
                               (setf head (list node)
                                     tail head)
                               (setf (cdr tail) (list node)
                                     tail (cdr tail))))
           (dequeue () (cond
                         ((null head)     nil)
                         ((eql head tail) (prog1 (car head)
                                            (setf head nil
                                                  tail nil)))
                         (t               (pop head))))
           (empty () (null head))
           (stop (node) (funcall predicate (funcall key node)))
           (mark (node) (setf (gethash node marks) t))
           (markedp (node) (gethash node marks)))
      (declare (inline enqueue dequeue empty stop mark markedp))
      (enqueue root)
      (mark root)
      (loop :until (empty) :do
         (let ((v (dequeue)))
           (when (stop v)
             (return-from breadth-first-search-if v))
           (loop :for w :in (graph-adjacency-list graph v) :do
              (unless (markedp w)
                (enqueue w)
                (mark w))))))))


(defstruct jug-graph
  capacities)

(defstruct jug-node
  contents
  path)

(defun target-state-p (node)
  (find 6 (jug-node-contents node)))

(defun graph-adjacency-list (graph node)
  ;; generate all possible transfers:
  ;; we skip when the source jug is empty,
  ;; or when the target jug is full.
  (let ((capacities (jug-graph-capacities graph))
        (contents   (jug-node-contents    node)))
    (loop
      :for i :from 0 :below (length capacities)
      :append (loop
                :for j :from 0 :below (length capacities)
                :when (and (/= i j)
                           (< 0 (aref contents i))
                           (< (aref contents j) (aref capacities j)))
                  :collect (make-jug-node
                            :contents (pour capacities contents i j)
                            :path     (cons (list contents i j)
                                            (jug-node-path node)))))))

(graph-adjacency-list
 (make-jug-graph :capacities #(12 8 5))
 (make-jug-node :contents #(12 0 0)))

;; --> (#S(jug-node :contents #(4 8 0) :path ((#(12 0 0) 0 1)))
;;      #S(jug-node :contents #(7 0 5) :path ((#(12 0 0) 0 2))))


(breadth-first-search-if (make-jug-graph :capacities #(12 8 5))
                         (make-jug-node :contents #(12 0 0))
                         (function target-state-p))
;; --> #S(jug-node :contents #(1 6 5)
;;                 :path ((#(1 8 3) 1 2)
;;                        (#(9 0 3) 0 1)
;;                        (#(9 3 0) 1 2)
;;                        (#(4 3 5) 2 0)
;;                        (#(4 8 0) 1 2)
;;                        (#(12 0 0) 0 1)))

(defun explain (node)
  (dolist (step (reverse (jug-node-path node)))
    (format t "With jug contents: ~S pour from jug ~A to jug ~A;~%"
            (first step) (second step) (third step)))
  (format t "Final jug contents: ~S~%" (jug-node-contents node)))

(explain #S(jug-node :contents #(1 6 5)
                     :path ((#(1 8 3) 1 2)
                            (#(9 0 3) 0 1)
                            (#(9 3 0) 1 2)
                            (#(4 3 5) 2 0)
                            (#(4 8 0) 1 2)
                            (#(12 0 0) 0 1))))
;; With jug contents: #(12 0 0) pour from jug 0 to jug 1;
;; With jug contents: #(4 8 0) pour from jug 1 to jug 2;
;; With jug contents: #(4 3 5) pour from jug 2 to jug 0;
;; With jug contents: #(9 3 0) pour from jug 1 to jug 2;
;; With jug contents: #(9 0 3) pour from jug 0 to jug 1;
;; With jug contents: #(1 8 3) pour from jug 1 to jug 2;
;; Final jug contents: #(1 6 5)

(explain (breadth-first-search-if (make-jug-graph :capacities #(10 7 3))
                         (make-jug-node :contents #(10 0 0))
                                  (lambda (node) (find 5 (jug-node-contents node)))))
;; With jug contents: #(10 0 0) pour from jug 0 to jug 1;
;; With jug contents: #(3 7 0) pour from jug 1 to jug 2;
;; With jug contents: #(3 4 3) pour from jug 2 to jug 0;
;; With jug contents: #(6 4 0) pour from jug 1 to jug 2;
;; With jug contents: #(6 1 3) pour from jug 2 to jug 0;
;; With jug contents: #(9 1 0) pour from jug 1 to jug 2;
;; With jug contents: #(9 0 1) pour from jug 0 to jug 1;
;; With jug contents: #(2 7 1) pour from jug 1 to jug 2;
;; Final jug contents: #(2 5 3)
